module codeberg.org/readeck/readeck

go 1.24

toolchain go1.24.1

require (
	github.com/CloudyKit/jet/v6 v6.3.1
	github.com/JohannesKaufmann/html-to-markdown v1.6.0
	github.com/PuerkitoBio/goquery v1.10.2
	github.com/antchfx/htmlquery v1.3.4
	github.com/antchfx/xmlquery v1.4.4
	github.com/anthonynsimon/bild v0.14.0
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de
	github.com/caarlos0/env/v11 v11.3.1
	github.com/casbin/casbin/v2 v2.103.0
	github.com/cristalhq/acmd v0.12.0
	github.com/cristalhq/jwt/v3 v3.1.0
	github.com/dop251/goja v0.0.0-20250309171923-bcd7cc6bf64c
	github.com/dop251/goja_nodejs v0.0.0-20250309172600-86a40d630cdd
	github.com/doug-martin/goqu/v9 v9.19.0
	github.com/gabriel-vasile/mimetype v1.4.8
	github.com/go-chi/chi/v5 v5.2.1
	github.com/go-redis/redis/v8 v8.11.5
	github.com/go-shiori/dom v0.0.0-20230515143342-73569d674e1c
	github.com/go-shiori/go-readability v0.0.0-20250217085726-9f5bf5ca7612
	github.com/google/uuid v1.6.0
	github.com/gorilla/csrf v1.7.2
	github.com/gorilla/securecookie v1.1.2
	github.com/hlandau/passlib v1.0.11
	github.com/itchyny/gojq v0.12.14
	github.com/jackc/pgx/v5 v5.7.2
	github.com/jarcoal/httpmock v1.3.1
	github.com/kinbiko/jsonassert v1.1.1
	github.com/klauspost/compress v1.18.0
	github.com/komkom/toml v0.1.2
	github.com/leonelquinteros/gotext v1.7.1
	github.com/lithammer/shortuuid/v4 v4.2.0
	github.com/mangoumbrella/goldmark-figure v1.0.0
	github.com/ncruces/go-sqlite3 v0.24.0
	github.com/phsym/console-slog v0.3.1
	github.com/prometheus/client_golang v1.21.1
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/stretchr/testify v1.10.0
	github.com/tdewolff/parse/v2 v2.7.20
	github.com/xhit/go-simple-mail/v2 v2.16.0
	github.com/yuin/goldmark v1.7.1
	github.com/yuin/goldmark-meta v1.1.0
	golang.org/x/image v0.25.0
	golang.org/x/net v0.37.0
	golang.org/x/sync v0.12.0
	golang.org/x/term v0.30.0
	golang.org/x/text v0.23.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/CloudyKit/fastprinter v0.0.0-20200109182630-33d98a066a53 // indirect
	github.com/andybalholm/cascadia v1.3.3 // indirect
	github.com/antchfx/xpath v1.3.3 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/bmatcuk/doublestar/v4 v4.8.1 // indirect
	github.com/casbin/govaluate v1.3.0 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/dlclark/regexp2 v1.11.5 // indirect
	github.com/go-sourcemap/sourcemap v2.1.4+incompatible // indirect
	github.com/go-test/deep v1.1.0 // indirect
	github.com/gogs/chardet v0.0.0-20211120154057-b7413eaefb8f // indirect
	github.com/golang/groupcache v0.0.0-20241129210726-2c02b8208cf8 // indirect
	github.com/google/pprof v0.0.0-20250302191652-9094ed2288e7 // indirect
	github.com/itchyny/timefmt-go v0.1.5 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20240606120523-5a60cdf6a761 // indirect
	github.com/jackc/puddle/v2 v2.2.2 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/mattn/go-sqlite3 v1.14.24 // indirect
	github.com/munnerz/goautoneg v0.0.0-20191010083416-a7dc8b61c822 // indirect
	github.com/ncruces/julianday v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/client_model v0.6.1 // indirect
	github.com/prometheus/common v0.62.0 // indirect
	github.com/prometheus/procfs v0.15.1 // indirect
	github.com/tetratelabs/wazero v1.9.0 // indirect
	github.com/toorop/go-dkim v0.0.0-20250226130143-9025cce95817 // indirect
	golang.org/x/crypto v0.36.0 // indirect
	golang.org/x/sys v0.31.0 // indirect
	google.golang.org/protobuf v1.36.5 // indirect
	gopkg.in/hlandau/easymetric.v1 v1.0.0 // indirect
	gopkg.in/hlandau/measurable.v1 v1.0.1 // indirect
	gopkg.in/hlandau/passlib.v1 v1.0.11 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
